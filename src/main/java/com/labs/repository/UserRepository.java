package com.labs.repository;

import com.labs.model.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;



public interface UserRepository extends ReactiveMongoRepository<User, Long> {
}
