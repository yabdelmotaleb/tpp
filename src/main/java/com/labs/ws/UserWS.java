package com.labs.ws;

import com.labs.dto.UserDto;
import com.labs.model.User;
import com.labs.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/users")
public class UserWS {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public Flux<UserDto> getUsers(){
        return userService.getUsers();
    }

    @PostMapping("/")
    @ResponseStatus(value = HttpStatus.CREATED)
    public void addUser(@RequestBody UserDto userDto){
        userService.addUser(userDto);
    }
}
