package com.labs.utils;

import com.labs.dto.UserDto;
import com.labs.model.User;

public class UserMapper {

    public static UserDto toDto(User user){
        UserDto userDto =new UserDto();
        userDto.setAge(user.getAge());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        return userDto;

    }


    public static User toEntity(UserDto userDto){
        User user =new User();
        user.setAge(userDto.getAge());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        return user;

    }
}
