package com.labs.service;

import com.labs.dto.UserDto;
import com.labs.model.User;
import com.labs.repository.UserRepository;
import com.labs.utils.UserMapper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserService implements IUser{

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void addUser(UserDto userDto) {
        userRepository.save(UserMapper.toEntity(userDto)).subscribe();
    }

    @Override
    public Flux<UserDto> getUsers() {
        return userRepository.findAll().map(user -> UserMapper.toDto(user));
    }
}
