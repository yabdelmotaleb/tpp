package com.labs.service;

import com.labs.dto.UserDto;
import reactor.core.publisher.Flux;


public interface IUser {

    void addUser(UserDto user);

    Flux<UserDto> getUsers();
}
