package com.labs.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.*;


import java.util.Date;


abstract class MongoAuditable  {

        @Id
        private ObjectId id;
        @CreatedBy
        private String createdBy;
        @CreatedDate
        private Date createdAt;
        @LastModifiedBy
        private String lastModifiedBy;
        @LastModifiedDate
        private Date lastModifiedAt;

        public ObjectId getId() {
                return id;
        }

        public MongoAuditable setId(ObjectId id) {
                this.id = id;
                return this;
        }

        public String getCreatedBy() {
                return createdBy;
        }

        public MongoAuditable setCreatedBy(String createdBy) {
                this.createdBy = createdBy;
                return this;
        }

        public Date getCreatedAt() {
                return createdAt;
        }

        public MongoAuditable setCreatedAt(Date createdAt) {
                this.createdAt = createdAt;
                return this;
        }

        public String getLastModifiedBy() {
                return lastModifiedBy;
        }

        public MongoAuditable setLastModifiedBy(String lastModifiedBy) {
                this.lastModifiedBy = lastModifiedBy;
                return this;
        }

        public Date getLastModifiedAt() {
                return lastModifiedAt;
        }

        public MongoAuditable setLastModifiedAt(Date lastModifiedAt) {
                this.lastModifiedAt = lastModifiedAt;
                return this;
        }
}

